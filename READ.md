# back-application

## Diagrama Contextual

![context diagram](./docs/Conntext-Diagram.png)

El sistema desarrollado cuenta con dos proyectos:
- Backend Application:
    - Desarrollo Spring Boot 
    - Uso de Base de Datos H2 (en memoria)
- Frontend Application:
    - Desarrollo React
    - Uso de Bootstrap
    - Uso de Material Design

## Diagrama de Clases

![class diagram](./docs/class-diagram.jpg)

El presente diagrama muestra el diseño del dominio involucrado en el sistema
- mascota:
    - Representacion de entidad mascota, datos que seran almacenados en el repositorio de datos
- dueno:
    -  Representacion de la entidad dueño, asociado a la mascota representada
- perro
    -  Extencion de la entidad mascota, representa las mascotas de la familia de Perros
- gato
    -  Extencion de la entidad mascota, representa las mascotas de la familia de Gatos

## Diagrama de Secuencia

![sequence diagram](./docs/sequence-diagram.jpg)

El presente diagrama representa la secuencia de llamadas realizadas al ejecutar  el endpoint:
- GET /mascotas

## Definición de la API

- [Documento OpenAPI 3](./docs/backend-api.yaml)

## Ejecucion de llamados a la API vía Postman

- [Postman Collection](./docs/APIMascotas.postman_collection.json)

## Construccion de la aplicación

```
mvn clean package
```

## Pruebas de la aplicación

```
mvn test
```

## Ejecución de la aplicación

```
mvn spring-boot:run
```
