package cl.basedos.prueba.backend.services;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import cl.basedos.prueba.backend.dao.DuenoDao;
import cl.basedos.prueba.backend.dao.MascotaDao;
import cl.basedos.prueba.backend.entity.DuenoEntity;
import cl.basedos.prueba.backend.entity.MascotaEntity;
import cl.basedos.prueba.backend.model.Mascota;
import cl.basedos.prueba.backend.services.impl.MascotaManagerImpl;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class MascotaManagerImplTests {
    @Mock
    private MascotaDao mascotaDao;

    @Mock
    private DuenoDao duenoDao;

    @InjectMocks
    MascotaManagerImpl mascotaManagerImpl;

    @Test
    public void whenListMascotas_thenReturnList() {

        List<MascotaEntity> mascotas =  new ArrayList<MascotaEntity>();

        DuenoEntity duenoEntity = new DuenoEntity();
        duenoEntity.setNombre("Juan");
        duenoEntity.setDireccion("algun  sitio");
        duenoEntity.setEmail("juan@pepemail.com");
        duenoEntity.setTelefono("987654321");

        MascotaEntity mascotaEntity = new MascotaEntity();
        mascotaEntity.setMascotaId(1);
        mascotaEntity.setNombre("princesa");
        mascotaEntity.setDescripcion("mi mascota");
        mascotaEntity.setEdad(1);
        mascotaEntity.setFamilia("perro");
        mascotaEntity.setTipo("toy");
        mascotaEntity.setDueno(duenoEntity);
        mascotas.add(mascotaEntity);

        Mockito.when(mascotaDao.findAll()).thenReturn(mascotas);
        Mockito.when(duenoDao.findByNombre(Mockito.anyString())).thenReturn(duenoEntity);

        List<Mascota> salida = null;
        salida = mascotaManagerImpl.listMascotas();

        assertNotNull(salida);
        assert(  salida.size() > 0 );
        assertNotNull( salida.get(0).getDueno() );
        
    }
    
}
