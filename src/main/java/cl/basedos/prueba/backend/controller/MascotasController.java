package cl.basedos.prueba.backend.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.NativeWebRequest;

import cl.basedos.prueba.backend.api.MascotasApi;
import cl.basedos.prueba.backend.model.AddMascota201Response;
import cl.basedos.prueba.backend.model.AddMascotaRequest;
import cl.basedos.prueba.backend.model.Mascota;
import cl.basedos.prueba.backend.services.MascotaManager;

@Controller
@RequestMapping("${openapi.swaggerMascota.base-path:}")
public class MascotasController implements MascotasApi {

    @Autowired
    private MascotaManager mascotaManager;

    @Override
    public ResponseEntity<AddMascota201Response> addMascota(AddMascotaRequest addMascotaRequest) {

        String familia  = addMascotaRequest.getFamilia();
        if (familia.equals("ave")) {
            return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
        }

        AddMascota201Response resp = mascotaManager.createMascota(addMascotaRequest);

        return ResponseEntity.ok(resp);
    }

    @Override
    public ResponseEntity<Void> deleteMascota(String idMascota) {

        mascotaManager.deleteMascota(idMascota);

        return ResponseEntity.ok(null);
    }

    @Override
    public ResponseEntity<Mascota> getMascota(String idMascota) {
        Mascota mascota =  mascotaManager.readMascota(idMascota);

        return ResponseEntity.ok(mascota);
    }

    @Override
    public Optional<NativeWebRequest> getRequest() {
        return MascotasApi.super.getRequest();
    }

    @Override
    public ResponseEntity<List<Mascota>> mascotasGet() {

        List<Mascota> tienda =  mascotaManager.listMascotas();

        return ResponseEntity.ok(tienda);
    }

    @Override
    public ResponseEntity<Mascota> putMascota(String idMascota, AddMascotaRequest addMascotaRequest) {

        Mascota mascota = mascotaManager.modifyMascota(idMascota, addMascotaRequest);

        
        return ResponseEntity.ok(mascota);
    }
    
}
