package cl.basedos.prueba.backend.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cl.basedos.prueba.backend.entity.DuenoEntity;
import cl.basedos.prueba.backend.entity.MascotaEntity;

@Repository
public interface MascotaDao extends CrudRepository<MascotaEntity, Integer> {

    public List<MascotaEntity> findAllByDueno(DuenoEntity dueno);
}