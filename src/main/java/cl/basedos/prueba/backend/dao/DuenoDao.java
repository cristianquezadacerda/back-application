package cl.basedos.prueba.backend.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cl.basedos.prueba.backend.entity.DuenoEntity;

@Repository
public interface DuenoDao extends CrudRepository<DuenoEntity, Integer> {

    public DuenoEntity findByNombre(String nombre);
}
