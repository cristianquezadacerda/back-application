package cl.basedos.prueba.backend.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Generated;

/**
 * Perro
 */


@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2024-05-10T10:01:24.611372-04:00[America/Santiago]")
public class Perro extends Mascota implements AddMascota201Response {

  /**
   * Gets or Sets tipo
   */
  public enum TipoEnum {
    TOY("toy"),
    
    PEQUENO("pequeno"),
    
    ESTANDAR("estandar"),
    
    GRANDE("grande"),
    
    GIGANTE("gigante");

    private String value;

    TipoEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static TipoEnum fromValue(String value) {
      for (TipoEnum b : TipoEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("tipo")
  private TipoEnum tipo;

  public Perro tipo(TipoEnum tipo) {
    this.tipo = tipo;
    return this;
  }

  /**
   * Get tipo
   * @return tipo
  */
  
  @Schema(name = "tipo", required = false)
  public TipoEnum getTipo() {
    return tipo;
  }

  public void setTipo(TipoEnum tipo) {
    this.tipo = tipo;
  }

  public Perro idMascota(String idMascota) {
    super.setIdMascota(idMascota);
    return this;
  }

  public Perro nombre(String nombre) {
    super.setNombre(nombre);
    return this;
  }

  public Perro edad(Integer edad) {
    super.setEdad(edad);
    return this;
  }

  public Perro descripcion(String descripcion) {
    super.setDescripcion(descripcion);
    return this;
  }

  public Perro familia(FamiliaEnum familia) {
    super.setFamilia(familia);
    return this;
  }

  public Perro dueno(Dueno dueno) {
    super.setDueno(dueno);
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Perro perro = (Perro) o;
    return Objects.equals(this.tipo, perro.tipo) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(tipo, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Perro {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    tipo: ").append(toIndentedString(tipo)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

