package cl.basedos.prueba.backend.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Generated;

/**
 * Gato
 */


@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2024-05-10T10:01:24.611372-04:00[America/Santiago]")
public class Gato extends Mascota implements AddMascota201Response {

  /**
   * Gets or Sets raza
   */
  public enum RazaEnum {
    SIAMES("siames"),
    
    ESFINGE("esfinge"),
    
    ANGORA("angora"),
    
    PERSA("persa"),
    
    OTRO("otro");

    private String value;

    RazaEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static RazaEnum fromValue(String value) {
      for (RazaEnum b : RazaEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("raza")
  private RazaEnum raza;

  public Gato raza(RazaEnum raza) {
    this.raza = raza;
    return this;
  }

  /**
   * Get raza
   * @return raza
  */
  
  @Schema(name = "raza", required = false)
  public RazaEnum getRaza() {
    return raza;
  }

  public void setRaza(RazaEnum raza) {
    this.raza = raza;
  }

  public Gato idMascota(String idMascota) {
    super.setIdMascota(idMascota);
    return this;
  }

  public Gato nombre(String nombre) {
    super.setNombre(nombre);
    return this;
  }

  public Gato edad(Integer edad) {
    super.setEdad(edad);
    return this;
  }

  public Gato descripcion(String descripcion) {
    super.setDescripcion(descripcion);
    return this;
  }

  public Gato familia(FamiliaEnum familia) {
    super.setFamilia(familia);
    return this;
  }

  public Gato dueno(Dueno dueno) {
    super.setDueno(dueno);
    return this;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Gato gato = (Gato) o;
    return Objects.equals(this.raza, gato.raza) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(raza, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Gato {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    raza: ").append(toIndentedString(raza)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

