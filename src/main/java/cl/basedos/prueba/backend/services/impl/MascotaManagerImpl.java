package cl.basedos.prueba.backend.services.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.basedos.prueba.backend.dao.DuenoDao;
import cl.basedos.prueba.backend.dao.MascotaDao;
import cl.basedos.prueba.backend.entity.DuenoEntity;
import cl.basedos.prueba.backend.entity.MascotaEntity;
import cl.basedos.prueba.backend.model.AddMascota201Response;
import cl.basedos.prueba.backend.model.AddMascotaRequest;
import cl.basedos.prueba.backend.model.Dueno;
import cl.basedos.prueba.backend.model.Gato;
import cl.basedos.prueba.backend.model.Gato.RazaEnum;
import cl.basedos.prueba.backend.model.Mascota;
import cl.basedos.prueba.backend.model.NuevaMascota;
import cl.basedos.prueba.backend.model.NuevaMascota.FamiliaEnum;
import cl.basedos.prueba.backend.model.NuevoGato;
import cl.basedos.prueba.backend.model.NuevoPerro;
import cl.basedos.prueba.backend.model.Perro;
import cl.basedos.prueba.backend.model.Perro.TipoEnum;
import cl.basedos.prueba.backend.services.MascotaManager;

@Service
public class MascotaManagerImpl implements MascotaManager {

    @Autowired
    private MascotaDao mascotaDao;

    @Autowired
    private DuenoDao duenoDao;

    @Override
    public List<Mascota> listMascotas() {
        List<Mascota> tienda = new ArrayList<Mascota>();

        Iterable<MascotaEntity> mascotas = mascotaDao.findAll();

        for (Iterator<MascotaEntity> it = mascotas.iterator(); it.hasNext(); ) {
            MascotaEntity entity = it.next();
            DuenoEntity duenoEntity = entity.getDueno();
            Dueno dueno = null;

            if (duenoEntity != null) {
                dueno  = new Dueno();
                dueno.setNombre(duenoEntity.getNombre());
                dueno.setDireccion(duenoEntity.getDireccion());
                dueno.setEmail(duenoEntity.getEmail());
                dueno.setTelefono(duenoEntity.getTelefono());
            }

            if (entity.getFamilia().equals("perro")) {
                Perro pet = new Perro();
                pet.setIdMascota("" + entity.getMascotaId());
                pet.setNombre(entity.getNombre());
                pet.setDescripcion(entity.getDescripcion());
                pet.setEdad(entity.getEdad());

                TipoEnum tipo = Perro.TipoEnum.fromValue(entity.getTipo());
                pet.setTipo(tipo);

                NuevaMascota.FamiliaEnum familia = NuevaMascota.FamiliaEnum.fromValue(entity.getFamilia());
                pet.setFamilia(familia);

                pet.setDueno(dueno);

                tienda.add(pet);
            } else if (entity.getFamilia().equals("gato")) {
                Gato pet = new Gato();
                pet.setIdMascota("" + entity.getMascotaId());
                pet.setNombre(entity.getNombre());
                pet.setDescripcion(entity.getDescripcion());
                pet.setEdad(entity.getEdad());

                RazaEnum raza = Gato.RazaEnum.fromValue(entity.getRaza());
                pet.setRaza(raza);

                NuevaMascota.FamiliaEnum familia = NuevaMascota.FamiliaEnum.fromValue(entity.getFamilia());
                pet.setFamilia(familia);

                pet.setDueno(dueno);

                tienda.add(pet);
            }

        }

        return tienda;
    }

    @Override
    public AddMascota201Response createMascota(AddMascotaRequest p_mascota) {

        AddMascota201Response mascota = null;

        DuenoEntity duenoEntity = null;

        if (p_mascota.getFamilia().equals("perro")) {

            NuevoPerro perro = (NuevoPerro) p_mascota;

            MascotaEntity mascotaEntity = new MascotaEntity();
            mascotaEntity.setNombre(perro.getNombre());
            mascotaEntity.setDescripcion(perro.getDescripcion());
            mascotaEntity.setEdad(perro.getEdad());
            mascotaEntity.setFamilia(perro.getFamilia());
            mascotaEntity.setDueno(null);

            if (perro.getDueno() != null)  {
                duenoEntity = duenoDao.findByNombre(perro.getDueno().getNombre());
                if (duenoEntity == null) {
                    duenoEntity = new DuenoEntity();
                    duenoEntity.setNombre(perro.getDueno().getNombre());
                    duenoDao.save(duenoEntity);
                }
            }
            mascotaEntity.setTipo(perro.getTipo().getValue());

            Perro out = (Perro) this.readMascota("" + mascotaEntity.getMascotaId());

            mascota  = (AddMascota201Response) out;
        } else if (p_mascota.getFamilia().equals("gato")) {
            NuevoGato gato = (NuevoGato) p_mascota;

            MascotaEntity mascotaEntity = new MascotaEntity();
            mascotaEntity.setNombre(gato.getNombre());
            mascotaEntity.setDescripcion(gato.getDescripcion());
            mascotaEntity.setEdad(gato.getEdad());
            mascotaEntity.setFamilia(gato.getFamilia());
            mascotaEntity.setDueno(null);

            if (gato.getDueno() != null)  {
                duenoEntity = duenoDao.findByNombre(gato.getDueno().getNombre());
                if (duenoEntity == null) {
                    duenoEntity = new DuenoEntity();
                    duenoEntity.setNombre(gato.getDueno().getNombre());
                    duenoDao.save(duenoEntity);
                }
            }

            mascotaEntity.setRaza(gato.getRaza().getValue());


            mascotaEntity.setDueno(duenoEntity);
            mascotaDao.save(mascotaEntity);

            Gato out = (Gato) this.readMascota("" + mascotaEntity.getMascotaId());

            mascota  = (AddMascota201Response) out;
        }


        return mascota;
    }

    @Override
    public Mascota readMascota(String p_mascotaId) {
        Mascota mascota  = null;

        MascotaEntity pet = mascotaDao.findById(Integer.parseInt(p_mascotaId)).get();

        Dueno dueno = null;

        if (pet != null) {
            if (pet.getFamilia().equals("perro")) {
                Perro perro = new Perro();

                perro.setIdMascota("" + pet.getMascotaId());
                perro.setNombre(pet.getNombre());
                perro.setEdad(pet.getEdad());
                perro.setDescripcion(pet.getDescripcion());

                FamiliaEnum familia = FamiliaEnum.fromValue(pet.getFamilia());
                perro.setFamilia(familia);

                TipoEnum tipo =  TipoEnum.fromValue(pet.getTipo());
                perro.setTipo(tipo);

                DuenoEntity duenoEntity  = pet.getDueno();
                if (duenoEntity != null)  {
                    dueno = new Dueno();
                    dueno.setNombre(duenoEntity.getNombre());
                    dueno.setDireccion(duenoEntity.getDireccion());
                    dueno.setEmail(duenoEntity.getEmail());
                    dueno.setTelefono(duenoEntity.getTelefono());
                }

                perro.setDueno(dueno);

                mascota = perro;
                
            } else if (pet.getFamilia().equals("gato")) {

                Gato gato = new  Gato();

                gato.setIdMascota("" + pet.getMascotaId());
                gato.setNombre(pet.getNombre());
                gato.setEdad(pet.getEdad());
                gato.setDescripcion(pet.getDescripcion());

                FamiliaEnum familia = FamiliaEnum.fromValue(pet.getFamilia());
                gato.setFamilia(familia);

                RazaEnum raza =  RazaEnum.fromValue(pet.getRaza());
                gato.setRaza(raza);

                DuenoEntity duenoEntity  = pet.getDueno();
                if (duenoEntity != null)  {
                    dueno = new Dueno();
                    dueno.setNombre(duenoEntity.getNombre());
                    dueno.setDireccion(duenoEntity.getDireccion());
                    dueno.setEmail(duenoEntity.getEmail());
                    dueno.setTelefono(duenoEntity.getTelefono());
                }

                gato.setDueno(dueno);

                mascota = gato;

            } else {
                throw new NotImplementedException();
            }
        }

        return mascota;
    }

    @Override
    public void deleteMascota(String p_mascota) {

        MascotaEntity mascota = mascotaDao.findById(Integer.parseInt(p_mascota)).get();

        mascotaDao.delete(mascota);
    }

    @Override
    public Mascota modifyMascota(String p_mascotaId, AddMascotaRequest p_addMascotaRequest) {
        Mascota pet = null;

        MascotaEntity mascotaEntity = mascotaDao.findById(Integer.parseInt(p_mascotaId)).get();

        if (p_addMascotaRequest.getFamilia().equals("perro")) {
            NuevoPerro nuevoPerro = (NuevoPerro) p_addMascotaRequest;

            mascotaEntity.setNombre(nuevoPerro.getNombre());
            mascotaEntity.setDescripcion(nuevoPerro.getDescripcion());
            mascotaEntity.setEdad(nuevoPerro.getEdad());
            mascotaEntity.setFamilia(nuevoPerro.getFamilia());
            mascotaEntity.setTipo(nuevoPerro.getTipo().getValue());

            mascotaDao.save(mascotaEntity);

        } else if (p_addMascotaRequest.getFamilia().equals("gato")) {
            NuevoGato nuevoGato = (NuevoGato) p_addMascotaRequest;

            mascotaEntity.setNombre(nuevoGato.getNombre());
            mascotaEntity.setDescripcion(nuevoGato.getDescripcion());
            mascotaEntity.setEdad(nuevoGato.getEdad());
            mascotaEntity.setFamilia(nuevoGato.getFamilia());
            mascotaEntity.setRaza(nuevoGato.getRaza().getValue());

            mascotaDao.save(mascotaEntity);
        } else {

        }

        pet = this.readMascota(p_mascotaId);


        return pet;
    }

}
