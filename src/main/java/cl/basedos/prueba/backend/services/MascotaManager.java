package cl.basedos.prueba.backend.services;

import java.util.List;

import cl.basedos.prueba.backend.model.AddMascota201Response;
import cl.basedos.prueba.backend.model.AddMascotaRequest;
import cl.basedos.prueba.backend.model.Mascota;

public interface MascotaManager {

    public List<Mascota> listMascotas();

    public AddMascota201Response createMascota(AddMascotaRequest p_mascota);

    public Mascota readMascota(String p_mascotaId);

    public Mascota modifyMascota(String p_mascotaId, AddMascotaRequest p_addMascotaRequest);

    public void deleteMascota(String p_mascotaId);
}