package cl.basedos.prueba.backend.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "Mascota")
public class MascotaEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="mascotaId", nullable=false)
    int mascotaId;

    @Column(name="nombre", nullable = false)
    String nombre;

    @Column(name="edad", nullable = false)
    int edad;

    @Column(name="descripcion", nullable = false)
    String descripcion;

    @Column(name="familia", nullable = false)
    String familia;

    @Column(name="raza", nullable = true)
    String raza;

    @Column(name="tipo", nullable = true)
    String tipo;

    @JoinColumn(name = "dueno")
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    DuenoEntity dueno;

    public DuenoEntity getDueno() {
        return dueno;
    }

    public void setDueno(DuenoEntity dueno) {
        this.dueno = dueno;
    }

    public int getMascotaId() {
        return mascotaId;
    }

    public void setMascotaId(int mascotaId) {
        this.mascotaId = mascotaId;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFamilia() {
        return familia;
    }

    public void setFamilia(String familia) {
        this.familia = familia;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
}
