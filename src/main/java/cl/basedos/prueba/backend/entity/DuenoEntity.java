package cl.basedos.prueba.backend.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "Dueno")
public class DuenoEntity {
    
    @Id
    @Column(name="nombre", nullable = false)
    String nombre;

    @Column(name="telefono", nullable = false)
    String telefono;

    @Column(name="email", nullable = false)
    String email;

    @Column(name="direccion", nullable = false)
    String direccion;

    public String getNombre() {
        return nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getEmail() {
        return email;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
