DROP TABLE IF EXISTS Dueno;

CREATE TABLE Dueno (
	nombre VARCHAR(20) PRIMARY KEY,
    telefono VARCHAR(9),
    email VARCHAR(100),
    direccion VARCHAR(200)
);

DROP TABLE IF EXISTS Mascota;

CREATE TABLE Mascota (
	mascotaId INT AUTO_INCREMENT PRIMARY KEY,
	nombre VARCHAR(20) NOT NULL,
    edad INT NOT NULL,
    descripcion VARCHAR(20) NOT NULL,
    familia VARCHAR(20) NOT NULL,
    raza VARCHAR(20),
    tipo VARCHAR(20),
    dueno VARCHAR(20)
);

ALTER TABLE Mascota
    ADD FOREIGN KEY (dueno) 
    REFERENCES Dueno(nombre);