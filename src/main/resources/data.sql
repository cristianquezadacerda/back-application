

INSERT INTO Dueno (nombre, telefono, email, direccion)  VALUES  ('Juan', '555555555', 'juan@pepemail.com', 'cerca');
INSERT INTO Dueno (nombre, telefono, email, direccion)  VALUES  ('Luis', '123456789', 'luis@pepemail.com', 'lejos');
INSERT INTO Dueno (nombre, telefono, email, direccion)  VALUES  ('Jose', '987654321', 'Jose@pepemail.com', 'otro lado');

INSERT INTO Mascota (nombre, edad, descripcion, familia, raza, tipo, dueno) VALUES ('Pepe', 3, 'El gato pepe', 'gato', 'siames', null, 'Juan');
INSERT INTO Mascota (nombre, edad, descripcion, familia, raza, tipo, dueno) VALUES ('Botines', 6, 'El con botas', 'gato', 'angora', null, 'Luis');
INSERT INTO Mascota (nombre, edad, descripcion, familia, raza, tipo, dueno) VALUES ('Joe', 4, 'Un perro grande', 'perro', null, 'grande', 'Jose');
INSERT INTO Mascota (nombre, edad, descripcion, familia, raza, tipo, dueno) VALUES ('Bobby', 1, 'Un perro pequeño', 'perro', null, 'toy', 'Luis');

